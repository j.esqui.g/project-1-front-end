import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import App from "./components/app"


ReactDOM.render(
  <React.StrictMode>
  <App></App>
  </React.StrictMode>,
  document.getElementById('root')
);

