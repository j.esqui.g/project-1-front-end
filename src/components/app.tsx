import React, {useState} from 'react';
import Chat from './chat';
import Planner from './planner';
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import axios from 'axios';

//https://www.freecodecamp.org/news/how-to-persist-a-logged-in-user-in-react/
// used this site to persist a logged in user in react

export default function App()
{
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useState<any>()

  function handleLogout() // re render cause we are setting again
  {
    setUser("");
    setEmail("");
    setPassword("");
    localStorage.clear();
  };

    // login the user
  async function handleSubmit(e:any)
  {
    e.preventDefault();
    // create user
    const user = { email, password };

    const response = await axios.patch("https://wedding-planner-325216.uk.r.appspot.com/users/login",user)

    setUser(response.data.fname) // was giving error when just using "response.data" maybe because the return is a json with 2 variables instead of 1. Had to break it down to 1 variable(fname)
    localStorage.setItem("user", JSON.stringify(response.data));
  };
    
   // if there's a user show the message below
   if (user){
    return (
      <div>
        {user} is logged in
        <button onClick={handleLogout}>logout</button>
        <BrowserRouter>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/planner">Planner</Link>
            </li>
            <li>
              <Link to="/chat">Chat</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/planner">
            <Planner />
          </Route>
          <Route path="/chat">
            <Chat />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
      </div>
    );
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="email">Email: </label>
        <input
          type="text"
          value={email}
          placeholder="Enter an Email"
          onChange={({ target }) => setEmail(target.value)}
        />
        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            value={password}
            placeholder="Enter a Password"
            onChange={({ target }) => setPassword(target.value)}
          />
        </div>
        <button type="submit">Login</button>
      </form>
    </div>
  );
}