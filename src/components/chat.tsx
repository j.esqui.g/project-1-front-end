import React, {useState, SyntheticEvent, useRef} from 'react';
import axios from 'axios';

// https://stackoverflow.com/questions/61556179/how-to-display-an-array-of-objects-in-a-table-in-react?rq=1
// based my table printing off of this link

export default function Chat()
{
    const headers:any[] = [{id: "",sender: "",recipient: "", note:"", timestamp: "" } ]
    const[messages,setMessages] = useState<any[]>( [ {id: "",sender: "",recipient: "", note:"", timestamp: "" } ]);
    
    const senderInputToSend = useRef<any>();
    const recipientInputToSend = useRef<any>();
    const messageToSend = useRef<any>();

    // id
    const idInput = useRef<any>();
    // only recipient
    const recipientInput = useRef<any>();
    // only sender
    const senderInput = useRef<any>();

    // sender and recipient
    const recipientInputTwo = useRef<any>()
    const senderInputTwo = useRef<any>()

    async function getAllMessages(event:SyntheticEvent)
    {
        const response = await axios.get("https://messaging-service-dot-wedding-planner-325216.uk.r.appspot.com/messages")
        const messages:any[] = response.data;

        let newMessages = [];
        for (let i = 0; i <= messages.length - 1; i++)
        {
          let obj = { id: "", sender: "", recipient: "", note: "", timestamp: ""};

          obj.id = messages[i].id;
          obj.sender = messages[i].sender;
          obj.recipient = messages[i].recipient;
          obj.note = messages[i].note;
          obj.timestamp = messages[i].timestamp;
        
          newMessages.push(obj);
        }
        setMessages(newMessages)
    }

    async function getMessageByID(event:SyntheticEvent)
    {
        const mid:number = idInput.current.value;
        const response = await axios.get(`https://messaging-service-dot-wedding-planner-325216.uk.r.appspot.com/messages/${mid}`)
        const messages:any[] = response.data;

        let newMessages = [];
        for (let i = 0; i <= messages.length - 1; i++)
        {
          let obj = { id: "", sender: "", recipient: "", note: "", timestamp: ""};

          obj.id = messages[i].id;
          obj.sender = messages[i].sender;
          obj.recipient = messages[i].recipient;
          obj.note = messages[i].note;
          obj.timestamp = messages[i].timestamp;
        
          newMessages.push(obj);
        }
        setMessages(newMessages)
    }
    async function getMessagesByRecipient(event:SyntheticEvent)
    {
        const recipient:string = recipientInput.current.value;
        const response = await axios.get(`https://messaging-service-dot-wedding-planner-325216.uk.r.appspot.com/messages?recipient=${recipient}`)
        const messages:any[] = response.data;

        let newMessages = [];
        for (let i = 0; i <= messages.length - 1; i++)
        {
          let obj = { id: "", sender: "", recipient: "", note: "", timestamp: ""};

          obj.id = messages[i].id;
          obj.sender = messages[i].sender;
          obj.recipient = messages[i].recipient;
          obj.note = messages[i].note;
          obj.timestamp = messages[i].timestamp;
        
          newMessages.push(obj);
        }
        setMessages(newMessages)
    }
    async function getMessagesBySender(event:SyntheticEvent)
    {
        const sender:string = senderInput.current.value;
        const response = await axios.get(`https://messaging-service-dot-wedding-planner-325216.uk.r.appspot.com/messages?sender=${sender}`)
        const messages:any[] = response.data;

        let newMessages = [];
        for (let i = 0; i <= messages.length - 1; i++)
        {
          let obj = { id: "", sender: "", recipient: "", note: "", timestamp: ""};

          obj.id = messages[i].id;
          obj.sender = messages[i].sender;
          obj.recipient = messages[i].recipient;
          obj.note = messages[i].note;
          obj.timestamp = messages[i].timestamp;
        
          newMessages.push(obj);
        }
        setMessages(newMessages)
    }

    async function getMessagesBySenderandRecipient(event:SyntheticEvent)
    {
        const sender:string = senderInputTwo.current.value;
        const recipient:string = recipientInputTwo.current.value;

        const response = await axios.get(`https://messaging-service-dot-wedding-planner-325216.uk.r.appspot.com/messages?sender=${sender}&recipient=${recipient}`)
        const messages:any[] = response.data;

        let newMessages = [];
        for (let i = 0; i <= messages.length - 1; i++)
        {
          let obj = { id: "", sender: "", recipient: "", note: "", timestamp: ""};

          obj.id = messages[i].id;
          obj.sender = messages[i].sender;
          obj.recipient = messages[i].recipient;
          obj.note = messages[i].note;
          obj.timestamp = messages[i].timestamp;
        
          newMessages.push(obj);
        }
        setMessages(newMessages)
    }
    
    async function createMessage(event:SyntheticEvent)
    { // only bug is that if we input a sender or recipient email that does not exist in database, nothing will get returned. This is a bug that has to be fixed in the Messaging Service
        const sender = senderInputToSend.current.value;
        const recipient = recipientInputToSend.current.value;
        const message = messageToSend.current.value;

        const body ={sender: sender, recipient: recipient, note: message}
        const response = await axios.post("https://messaging-service-dot-wedding-planner-325216.uk.r.appspot.com/messages",body)
        // come back and put something here to check response 
    }


    return(<div>
        <h2>Chat Page</h2>

        <div>
          <button onClick={createMessage}>Create a message</button>
          <input placeholder="Sender" ref={senderInputToSend}></input>
          <input placeholder="Recipient" ref={recipientInputToSend}></input>
          <input placeholder="Message" ref={messageToSend}></input>
        </div>

        <div>
          <button onClick={getAllMessages}>Get Messages</button>
        </div>

        <div>
          <button onClick ={getMessageByID}>Get Message by ID</button>
          <input placeholder="Message ID" ref={idInput}></input>
        </div>

        <div>
          <button onClick={getMessagesByRecipient}> Get Messages by Recipient </button>
          <input placeholder="Recipient" ref={recipientInput}></input>
        </div>

        <div>
          <button onClick={getMessagesBySender}>Get Messages by Sender </button>
          <input placeholder="Sender" ref={senderInput}></input>
        </div>

        <div>
          <button onClick={getMessagesBySenderandRecipient}>Get Msgs by Sender and Recipient</button>
          <input placeholder="Sender" ref={senderInputTwo}></input>
          <input placeholder="Recipient" ref={recipientInputTwo}></input>
        </div>
        

        <table>
            <tr key={"header"}> {Object.keys(headers[0]).map((key) => (<th>{key}</th>))} </tr>
            {messages.map( (item) =>
            (
            <tr key={item.id}> {Object.values(item).map((val:any) => (<td>{val}</td>))} </tr>
            )
            )
            }
        </table>

    </div>

    )
}