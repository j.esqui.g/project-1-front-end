import React, {useState, SyntheticEvent, useRef} from 'react';
import axios from 'axios';

export default function Planner()
{
    // Weddings

    // 0
    const weddingDate0 = useRef<any>()
    const weddingLocation0 = useRef<any>();
    const weddingName0 = useRef<any>()
    const weddingBudget0 = useRef<any>()
    // 1 nothing

    // 2
    const weddingIDInput = useRef<any>();
    // 3
    const weddingID3 = useRef<any>();
    const weddingDate3 = useRef<any>()
    const weddingLocation3 = useRef<any>();
    const weddingName3 = useRef<any>()
    const weddingBudget3 = useRef<any>()

    // 4
    const weddingID4 = useRef<any>()

    // Expenses

    // 0
    const reason0 = useRef<any>()
    const amount0 = useRef<any>()
    const weddingid0 = useRef<any>()

    // 1 nothing

    // 2
    const weddingid2 =useRef<any>()
    // 3
    const expenseid3 = useRef<any>()
    // 4 
    const expenseid4 = useRef<any>()
    const reason4 = useRef<any>();
    const amount4 = useRef<any>();
    const photo4 = useRef<any>();
    const weddingid4 = useRef<any>();
    // 5
    const expenseid5 = useRef<any>()

    // file upload
    const filename = useRef<any>();
    const filetype = useRef<any>();

    // Headers
    const weddingheaders:any[] = [{Wedding_ID: "",Date: "",Location: "", Name:"", Budget: "" } ]
    const expenseheaders:any[] = [{Expense_ID: "",Reason: "",Amount: "", Photo:"", Wedding_ID: "" } ]

    const[weddings,setWeddings] = useState<any[]>( [ {wedding_id: "",wedding_date: "",wedding_location: "", wedding_name:"", wedding_budget: "" } ]);
    const[expenses,setExpenses] = useState<any[]>( [ {expense_id: "",reason: "",amount: "", photo:"", wedding_id_fk: "" } ]);

    // Base64
    const [baseImage,setBaseImage] = useState<any>("")


    async function createWedding(event:SyntheticEvent)
    {
        const wedding_date = weddingDate0.current.value;
        const wedding_location = weddingLocation0.current.value;
        const wedding_name = weddingName0.current.value;
        const wedding_budget = weddingBudget0.current.value;

        const body ={wedding_id:0, wedding_date:wedding_date, wedding_location: wedding_location, wedding_name: wedding_name, wedding_budget: wedding_budget}

        const response = await axios.post("http://35.245.161.167:3000/weddings",body)
        const weddings:any = response.data;
        let newWeddings= [];
        newWeddings.push(weddings);
        setWeddings(newWeddings)
    }

    // 1
    async function getWeddings(event:SyntheticEvent)
    {
        const response = await axios.get("http://35.245.161.167:3000/weddings")
        const weddings:any[] = response.data;
        setWeddings(weddings)
    }
    // 2
    async function getWeddingByID(event:SyntheticEvent)
    {
        const id = weddingIDInput.current.value;
        const response = await axios.get(`http://35.245.161.167:3000/weddings/${id}`)
        const weddings:any = response.data;
        console.log(weddings)
        let newWeddings= [];
        newWeddings.push(weddings);
        setWeddings(newWeddings)

    }
    // 3
    async function updateWeddingWithIDOf(event:SyntheticEvent)
    {
        const id = weddingID3.current.value;
        const wedding_date = weddingDate3.current.value;
        const wedding_location = weddingLocation3.current.value;
        const wedding_name = weddingName3.current.value;
        const wedding_budget = weddingBudget3.current.value;

        const body ={wedding_id:0, wedding_date:wedding_date, wedding_location: wedding_location, wedding_name: wedding_name, wedding_budget: wedding_budget}

        const response = await axios.put(`http://35.245.161.167:3000/weddings/${id}`,body)
        const weddings:any = response.data
        let newWeddings= [];
        newWeddings.push(weddings);
        setWeddings(newWeddings)

    }
    // 4
    async function deleteWeddingWithIDof(event:SyntheticEvent)
    {
        const id = weddingID4.current.value;

        const response = await axios.delete(`http://35.245.161.167:3000/weddings/${id}`)
        const weddings:any = response.data;
        console.log(weddings)
        let newWeddings= [];
        newWeddings.push(weddings);
        setWeddings(newWeddings)
        // console.log("got here")
        // const weddings:any = response.status;
        // console.log(weddings)
        // if(weddings === 404 || weddings === "404")
        // {
        //     const body = {wedding_id: `wedding id:${id}`,wedding_date: " Does ",wedding_location: "not", wedding_name:"exist", wedding_budget: "currently" }
        //     let newWeddings = []
        //     newWeddings.push(body)
        //     setWeddings(newWeddings)
        // }
        // else
        // {
        //     const body = {wedding_id: `wedding id:${id}`,wedding_date: " Has ",wedding_location: "now ", wedding_name:"been ", wedding_budget: "deleted" }
        //     let newWeddings = []
        //     newWeddings.push(body)
        //     setWeddings(newWeddings)
        // }

    }

    // 0
    async function createExpense(event:SyntheticEvent)
    {
        const expense_id = 0;
        const reason = reason0.current.value;
        const amount = amount0.current.value;
        const wedding_id_fk = weddingid0.current.value;
        let photo = "-";

        if(baseImage.length > 1) // expense with picture to get link
        {
            const content = baseImage.substring(baseImage.indexOf(',') + 1) // get base64 string // https://stackoverflow.com/questions/573145/get-everything-after-the-dash-in-a-string-in-javascript/35236900
            const name = filename.current.value;
            const extension = filetype.current.value;
            const body = {name: name, extension: extension, content: content}
            const response = await axios.post("https://us-east4-wedding-planner-325216.cloudfunctions.net/file-upload",body)
            photo = response.data.photoLink;
            console.log(photo)
        }

            const body ={expense_id:expense_id, reason: reason, amount: amount, photo: photo, wedding_id_fk: wedding_id_fk}
    
            const response = await axios.post("http://35.245.161.167:3000/expenses",body)
            const expenses:any = response.data;
            let newExpenses= [];
            newExpenses.push(expenses);
            setExpenses(newExpenses)
            setBaseImage("") // reset
        
    }
    // 1
    async function getExpenses(event:SyntheticEvent)
    {
        const response = await axios.get("http://35.245.161.167:3000/expenses")
        const expenses:any[] = response.data;
        setExpenses(expenses)
    }
    // 2
    async function getExpensesForWeddingWithIDOf(event:SyntheticEvent)
    {
        const id = weddingid2.current.value;
        const response = await axios.get(`http://35.245.161.167:3000/weddings/${id}/expenses`)
        const expenses:any[] = response.data;
        setExpenses(expenses)
 
    }
    // 3
    async function getExpenseByID(event:SyntheticEvent)
    {// when dealing with only one record coming back, there seems to be a problem which makes me have to create a new array and then push data into the new array. then push the new array into setexpenses
        const id = expenseid3.current.value;
        const response = await axios.get(`http://35.245.161.167:3000/expenses/${id}`)
        const expenses:any = response.data;
        console.log(expenses)
        let newExpenses= [];
        newExpenses.push(expenses);
        setExpenses(newExpenses)
    }
    // 4
    async function updateExpenseWithIDOf(event:SyntheticEvent)
    {
        const expense_id = expenseid4.current.value;
        const reason = reason4.current.value;
        const amount = amount4.current.value;
        const photo = photo4.current.value;
        const wedding = weddingid4.current.value;

        const body ={expense_id:0, reason: reason, amount:amount, photo:photo,wedding_id_fk:wedding}

        const response = await axios.put(`http://35.245.161.167:3000/expenses/${expense_id}`,body)
        const expenses:any = response.data // returns only 1, so expenses name may be confusing
        let newExpenses= [];
        newExpenses.push(expenses);
        setExpenses(newExpenses)

    }
    // 5
    async function deleteExpenseWithIDOf(event:SyntheticEvent)
    {
        const id = expenseid5.current.value;

        const response = await axios.delete(`http://35.245.161.167:3000/expenses/${id}`)

        const expenses:any = response.data;
        console.log(expenses)
        let newExpenses= [];
        newExpenses.push(expenses);
        setExpenses(newExpenses)
    }

    // these 2 functions made possible with https://www.youtube.com/watch?v=qmr9NCYjueM&ab_channel=SamLama
    const uploadImage = async(e:any) =>{
        const file = e.target.files[0];
        const base64 = await convertBase64(file)
        setBaseImage(base64)
    }
    function convertBase64(file:any)
    {
        return new Promise((resolve,reject)=>{

            const fileReader = new FileReader()
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            }

            fileReader.onerror = (error) => {
                reject(error);
            }
        })
    }

    return(
        <div>
        <h2>Planner Page</h2>
        {/* 0 */} 
        <div>
            <button onClick={createWedding}>Create a Wedding</button>
            <input placeholder="Wedding Date" ref={weddingDate0}></input>
            <input placeholder="Wedding Location" ref={weddingLocation0}></input>
            <input placeholder="Wedding Name" ref={weddingName0}></input>
            <input placeholder="Wedding Budget" ref={weddingBudget0}></input>
        </div>
        {/* 1 */} 
        <div>
            <button onClick={getWeddings}>Get all Weddings</button>
        </div>
        {/* 2 */} 
        <div>
            <button onClick={getWeddingByID}>Get Weddings By ID</button>
            <input placeholder="Wedding ID" ref={weddingIDInput}></input>
        </div>
        {/* 3 */} 
        <div>
            <button onClick={updateWeddingWithIDOf}>Update Wedding with ID of</button>
            <input placeholder="Wedding ID" ref={weddingID3}></input>
            <input placeholder="Wedding Date" ref={weddingDate3}></input>
            <input placeholder="Wedding Location" ref={weddingLocation3}></input>
            <input placeholder="Wedding Name" ref={weddingName3}></input>
            <input placeholder="Wedding Budget" ref={weddingBudget3}></input>
        </div>
        {/* 4 */} 
        <div>
            <button onClick={deleteWeddingWithIDof}>Delete Wedding with ID of</button>
            <input placeholder="Wedding ID" ref={weddingID4}></input>
        </div>
        {/* Table */} 
        <table>
            <tr key={"header"}> {Object.keys(weddingheaders[0]).map((key) => (<th>{key}</th>))} </tr>
            {weddings.map((item) =>
            (
            <tr key={item.id}> {Object.values(item).map((val:any) => (<td>{val}</td>))}</tr>
            )
            )
            }
        </table>
        
        <br></br>
        <br></br>
        <br></br>

        <div>
            <p>When creating an Expense please fill out Reason, Amount, and Wedding_ID. If uploading a picture ALONG WITH the expense, please fill out FileName, File Type/Extension, and Upload image as well. Always reset after creating an expense</p>
            <button onClick={createExpense}>Create an Expense</button>
            <input placeholder="Reason" ref={reason0}></input>
            <input placeholder="Amount" ref={amount0}></input>
            <input placeholder="Wedding_ID" ref={weddingid0}></input>
            <span style={{fontSize:25}}><b>|</b></span>

            <form >
            <input placeholder="FileName" ref={filename}></input>
            <input placeholder="File Type/Extension" ref={filetype}></input>
            <input type="file" name="file" onChange={(e) => {uploadImage(e)}}/>
            <input type="reset" value = "Reset"/>
            </form>
        </div>

        <br></br>

        <div>
            <button onClick={getExpenses}>Get all Expenses</button>
        </div>
        <div>
            <button onClick ={getExpensesForWeddingWithIDOf}>Get Expenses for Wedding of ID of</button>
            <input placeholder="Wedding_ID" ref={weddingid2}></input>
        </div>
        <div>
            <button onClick={getExpenseByID}>Get Expenses by ID</button>
            <input placeholder="Expense_ID" ref={expenseid3}></input>
        </div>
        <div>
            <button onClick={updateExpenseWithIDOf}>Update Expense with ID of</button>
            <input placeholder="Expense_ID" ref={expenseid4}></input>
            <input placeholder="Reason" ref={reason4}></input>
            <input placeholder="Amount" ref={amount4}></input>
            <input placeholder="Photo" ref={photo4}></input>
            <input placeholder="Wedding_ID" ref={weddingid4}></input>
        </div>
        <div>
            <button onClick={deleteExpenseWithIDOf}>Delete Expense with ID of</button>
            <input placeholder="Expense_ID" ref={expenseid5}></input>
        </div>

        <table>
            <tr key={"header"}> {Object.keys(expenseheaders[0]).map((key) => (<th>{key}</th>))} </tr>
            {expenses.map((item) =>
            (
            <tr key={item.id}> {Object.values(item).map((val:any) => (<td>{val}</td>))} </tr>
            )
            )
            }
        </table>
    </div>
    )
}